climates
{
	test_climate ;; Subtropical desert
	sandy_desert ;; Desert floodplain
	rocky_desert ;; Semi-arid
	temperate_grassland_fertile ;; Mediterranean farmland
	temperate_grassland_infertile ;; Temperate desert
	temperate_forest_open ;; Steppe
	temperate_forest_deep ;; Temperate farmland
	swamp ;; Subtropical rainforest
	highland ;; Hymalaian cedar forest
	alpine ;; Temperate wetland
	sub_arctic ;; Temperate forest
	semi_arid ;; Levant cedar forest
}

climate test_climate ;; subtropical desert
{
	colour	255 153 102
	heat	4
	winter ;; not actually winter, this is where sand storms happen. Idem ditto for all other occurences of "winter". In descr_daytypes, this "season" has different levels of sand storms and otherwise a red haze at the horizon. Perhaps also a few days of very harsh sunshine with some dust clouds flying around

	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/desert_plant_1.cas			9
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/desert_plant_2.cas			8
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/desert_plant_3.cas			9
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_rock_1.cas	8
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_rock_2.cas	9
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_rock_3.cas	8
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_rock_4.cas	9
	strategy	summer	sparse_tree	palm_a.cas	8
	strategy	summer	sparse_tree	palm_b.cas	9
	strategy	summer	dense_tree	olive_a_impassable.cas	10
	strategy	summer	canopy		../../../../ax/data/terrain/aerial_map/tree_models/alx_olive_a_canopy.cas		12

	battle_vegetation
	dense_forest		ALX_semi_arid_shrubland
	dense_scrub		ALX_semi_arid_scrub

	battle_winter_vegetation
	dense_forest		ALX_semi_arid_shrubland_win
	dense_scrub		ALX_semi_arid_scrub_win_snow
	env_map			data/battlefield/envmaps/grass.dds
}

climate sandy_desert ;; desert floodplain
{
	colour	105 140 71
	heat	4

	strategy	summer	sparse_tree	palm_a.cas	8
	strategy	summer	sparse_tree	palm_b.cas	9
	strategy	summer	dense_tree	olive_a_impassable.cas	10
	strategy	summer	canopy		../../../../ax/data/terrain/aerial_map/tree_models/alx_olive_a_canopy.cas		12

	battle_vegetation
	dense_forest		ALX_temperate_and_cold_desert_rock
	dense_scrub		ALX_desert_scrub

	battle_winter_vegetation
	dense_forest		ALX_temperate_and_cold_desert_rock
	dense_scrub		ALX_desert_scrub
	env_map			data/battlefield/envmaps/grass.dds
}

climate rocky_desert ;; semi-arid
{
	colour	191 139 86
	heat	4
	winter

	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/desert_plant_1.cas			9
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/desert_plant_2.cas			8
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/desert_plant_3.cas			9
	strategy	summer	sparse_tree	palm_a.cas	8
	strategy	summer	sparse_tree	palm_b.cas	9
	strategy	summer	dense_tree	olive_a_impassable.cas	10
	strategy	summer	canopy		../../../../ax/data/terrain/aerial_map/tree_models/alx_olive_a_canopy.cas		12

	battle_vegetation
	dense_forest		ALX_hot_desert_palms
	dense_scrub		ALX_desert_scrub

	battle_winter_vegetation
	dense_forest		ALX_hot_desert_palms_win
	dense_scrub		ALX_desert_scrub
	env_map			data/battlefield/envmaps/grass.dds
}

climate temperate_grassland_fertile ;; mediterranean farmland
{
	colour	129 140 70
	heat	2

	strategy	summer	sparse_tree	olive_a.cas				10
	strategy	summer	dense_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_deciduous_a_impassable.cas			0
	strategy	summer	canopy		../../../../ax/data/terrain/aerial_map/tree_models/alx_deciduous_a_canopy.cas				12

	battle_vegetation
	dense_forest	ALX_semi_arid_shrubland
	dense_scrub		ALX_semi_arid_scrub

	battle_winter_vegetation
	dense_forest	ALX_semi_arid_shrubland
	dense_scrub		ALX_semi_arid_scrub_win
	env_map			data/battlefield/envmaps/grass.dds
}

climate temperate_grassland_infertile ;; temperate desert
{
	colour	217 157 98
	heat	3
	winter

	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/desert_plant_1.cas			10
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/desert_plant_2.cas			10
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/desert_plant_3.cas			10
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_rock_1.cas	10
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_rock_2.cas	10
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_rock_3.cas	10
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_rock_4.cas	10
	strategy	summer	dense_tree	olive_a_impassable.cas	10
	strategy	summer	canopy		../../../../ax/data/terrain/aerial_map/tree_models/alx_olive_a_canopy.cas		12

	battle_vegetation
	dense_forest	ALX_hot_semi_arid_palms
	dense_scrub		ALX_semi_arid_scrub

	battle_winter_vegetation
	dense_forest	ALX_hot_semi_arid_palms_win
	dense_scrub		ALX_semi_arid_scrub
	env_map			data/battlefield/envmaps/grass.dds
}

climate temperate_forest_open ;; steppe
{
	colour	166 159 82
	heat	2

	strategy	summer	sparse_tree	birch_a.cas						1
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_birch_a.cas						1
	strategy	summer	dense_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_birch_a_impassable.cas			0
	strategy	summer	canopy		../../../../ax/data/terrain/aerial_map/tree_models/alx_birch_a_canopy.cas				12

	battle_vegetation
	dense_forest	ALX_medi_forest
	dense_scrub		ALX_grain

	battle_winter_vegetation
	dense_forest	ALX_medi_forest_win
	dense_scrub		ALX_grain_win
	env_map			data/battlefield/envmaps/grass.dds
}

climate temperate_forest_deep ;; temperate farmland
{
	colour	130 153 84
	heat	1

	strategy	summer	sparse_tree	deciduous_a.cas						1
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_deciduous_a.cas						1
	strategy	summer	dense_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_deciduous_a_impassable.cas			0
	strategy	summer	canopy		../../../../ax/data/terrain/aerial_map/tree_models/alx_deciduous_a_canopy.cas				12

	battle_vegetation
	dense_forest	ALX_subtropical_rainforest
	dense_scrub		ALX_subtropical_rainforest_scrub

	battle_winter_vegetation
	dense_forest	ALX_subtropical_rainforest_win
	dense_scrub		ALX_subtropical_rainforest_scrub
	env_map			data/battlefield/envmaps/grass.dds
}

climate swamp ;; subtropical rainforest
{
	colour	54 153 54
	heat	4

	strategy	summer	sparse_tree	deciduous_a.cas						1
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_deciduous_a.cas						1
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_deciduous_b.cas						1
	strategy	summer	dense_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_deciduous_a_impassable.cas			0
	strategy	summer	canopy		../../../../ax/data/terrain/aerial_map/tree_models/alx_deciduous_a_canopy.cas				12

	battle_vegetation
	dense_forest		ALX_desert_floodland_forest
	dense_scrub		ALX_grain

	battle_winter_vegetation
	dense_forest		ALX_desert_floodland_forest_win
	dense_scrub		ALX_grain_win
	env_map			data/battlefield/envmaps/grass.dds
}

climate highland ;; himalayan cedar forest
{
	colour	51 102 60
	heat	2

	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_cedar_a.cas						1
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_cedar_b.cas						1
	strategy	summer	dense_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_cedar_a_impassable.cas			0
	strategy	summer	dense_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_cedar_b_impassable.cas			0
	strategy	summer	canopy		../../../../ax/data/terrain/aerial_map/tree_models/alx_cedar_a_canopy.cas				12
	strategy	summer	canopy		../../../../ax/data/terrain/aerial_map/tree_models/alx_cedar_b_canopy.cas				12

	battle_vegetation
	dense_forest	ALX_temperate_steppe_sparse_woodland
	dense_scrub		ALX_temperate_steppe_grassland

	battle_winter_vegetation
	dense_forest	ALX_temperate_steppe_sparse_woodland_win
	dense_scrub		ALX_temperate_steppe_grassland_win
	env_map			data/battlefield/envmaps/grass.dds
}

climate alpine ;; temperate wetland
{
	colour	57 115 57
	heat	1

	strategy	summer	sparse_tree	birch_a.cas						1
	strategy	summer	sparse_tree	deciduous_a.cas						1
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_birch_a.cas						1
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_deciduous_a.cas						1
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_deciduous_b.cas						1
	strategy	summer	dense_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_birch_a_impassable.cas			0
	strategy	summer	dense_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_deciduous_a_impassable.cas			0
	strategy	summer	canopy		../../../../ax/data/terrain/aerial_map/tree_models/alx_birch_a_canopy.cas				12
	strategy	summer	canopy		../../../../ax/data/terrain/aerial_map/tree_models/alx_deciduous_a_canopy.cas				12

	battle_vegetation
	dense_forest	ALX_temperate_deciduous_forest
	dense_scrub		ALX_temperate_broadleaf_forest_scrub_win

	battle_winter_vegetation
	dense_forest		ALX_temperate_deciduous_forest_win
	dense_scrub		ALX_temperate_broadleaf_forest_scrub_win
	env_map			data/battlefield/envmaps/grass.dds
}

climate sub_arctic ;; temperate forest
{
	colour	89 128 51
	heat	1

	strategy	summer	sparse_tree	birch_a.cas						1
	strategy	summer	sparse_tree	deciduous_a.cas						1
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_birch_a.cas						1
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_deciduous_a.cas						1
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_deciduous_b.cas						1
	strategy	summer	dense_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_birch_a_impassable.cas			0
	strategy	summer	dense_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_conifer_a_impassable.cas			3
	strategy	summer	canopy		../../../../ax/data/terrain/aerial_map/tree_models/alx_birch_a_canopy.cas				12
	strategy	summer	canopy		../../../../ax/data/terrain/aerial_map/tree_models/alx_conifer_a_canopy.cas				14

	battle_vegetation
	dense_forest	ALX_temperate_deciduous_forest
	dense_scrub		ALX_grain

	battle_winter_vegetation
	dense_forest	ALX_temperate_deciduous_forest_win
	dense_scrub		ALX_grain_win
	env_map			data/battlefield/envmaps/grass.dds
}

climate semi_arid ;; Levant cedar forest
{
	colour	51 102 68
	heat	3

	strategy	summer	sparse_tree	cypress_a.cas			11
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_cedar_a.cas						1
	strategy	summer	sparse_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_cedar_b.cas						1
	strategy	summer	dense_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_cedar_a_impassable.cas			0
	strategy	summer	dense_tree	../../../../ax/data/terrain/aerial_map/tree_models/alx_cedar_b_impassable.cas			0
	strategy	summer	canopy		../../../../ax/data/terrain/aerial_map/tree_models/alx_cedar_a_canopy.cas				12
	strategy	summer	canopy		../../../../ax/data/terrain/aerial_map/tree_models/alx_cedar_b_canopy.cas				12

	battle_vegetation
	dense_forest	ALX_temperate_and_cold_desert_rock
	dense_scrub		ALX_desert_scrub

	battle_winter_vegetation
	dense_forest	ALX_cold_desert_rock_win
	dense_scrub		ALX_desert_scrub
	env_map			data/battlefield/envmaps/grass.dds
}

