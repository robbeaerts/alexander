SigniferOne's Animations Pack, v0.8 Beta



Permissions:
This mod is available without restrictions for all personal use. Any other use, such as inclusion in other mods, requires explicit permission and is otherwise prohibited.



What's new:
- Complete conversion for fs_s1_gladius. All animations have been converted,
except walking/running anims, and of course turning during battle.
-- charge with gladius held level, not waving above the head
-- block high/block low, dodge high/dodge low much improved
-- matching "impact"/getting hit anims
-- appropriate attack during run
-- hold shield if knocked down
-- no taunting of enemy before battle
-- non-static "idle" animation, fully matching idle animations now
-- etc

- Complete conversionfor fs_s1_hoplite. All animations have been converted,
except turning during battle.
-- Fixed charge, does not skip any longer
-- Minimized skipping during attacks
-- block high/block low, dodge high/dodge low much improved
-- matching "impact"/getting hit anims
-- appropriate attack during run
-- no taunting of the enemy before battle
-- non-static "idle" animation
-- etc

- New faster and smoother javelinman animations, 
-- improved efficiency of Romans and of skirmishers
-- animation is smoother and quicker to respond

- Relaxed swordsman stance available for all swordsmen, not just fs_s1_gladius
-- completely fixed (non jerky) and very smooth idle animations
-- non static, slightly moving "stand" animation

- a total of over 300 animations added to the game
-- All animation skeletons upgraded to be compatible with RTW 1.5/1.6
-- Speed variants for all infantry skeletons



Description:
The following animation skeletons are now available to be used:

FS_S1_HOPLITE (variants: fs_s1_slow_hoplite)
- no wild taunting
- classic hoplite stand position
- fully compatible charge

FS_S1_GLADIUS (variants: fs_s1_slow_gladius)
- no wild taunting for Romans during startup
- during a fight, holding the shield close and the gladius ready next to it
- stabbing fighting style with the gladius, no slashing

FS_S1_SWORDSMAN (variants: fs_s1_fast_swordsman, fs_s1_semi_fast_swordsman, fs_s1_slow_swordsman)
- holding the sword relaxed
- smooth idle animations
- non-static "stand" animation
- no "feint" animation to make fighting less jerky and more smooth

fs_S1_BARBSWORDSMAN (variants: fs_s1_fast_barbswordsman, fs_s1_semi_fast_barbswordsman, fs_s1_slow_barbswordsman)
- everything included in fs_s1_swordsman
- no thrust for barbarians, only big slashes, as their swords were not designed
for thrusts and even had blunt points

FS_S1_ARCHER (variants: fs_s1_fast_archer, fs_s1_semi_fast_archer)
- aiming vertically to the sky, rather than horizontally, and fully drawing the bow

FS_S1_HC_ARCHER
- when aiming, holding the bow vertically rather than horizontally, and drawing
the bow with full power

FS_S1_FOREST_ELEPHANT_JAVELINRIDER
- javelinmen can stand on elephants and shoot javelins
- good complement to charion javelinman skeleton in 1.5/1.6





Where to download:
http://www.twcenter.net/downloads/db/index.php?mod=464



How to install:
Unzip the four files into Data/animations folder, overwriting the existing four files 
there (always remember to back everything up!).



How to use:
Go into descr_model_battle.txt and change which units use which skeletons (i.e. give fs_s1_hc_archer to Parthian horse archers, change Roman infantry from fs_javelinman, fs_swordsman to fs_javelinman, fs_s1_gladius, etc).

Supplementary note: I strongly recommend that for using fs_s1_hoplite, you remove the soldier's secondary weapon (sword), and also remove the "phalanx" ability; however giving them "shield wall" works great. This is for best results. To do all this, open export_descr_unit.txt, search for your hoplite unit, change his secondary weapon stats to 0, 0 (the rest doesn't matter), delete the word "phalanx" from his abilities (and give "shield wall"). fs_s1_hoplite works really well with this combination.

This version of the mod is released to be 100% stable, and is not expected to interfere
with any other part of the game. So, as always, remember to describe what goes wrong, 
if it does (even if I don't expect anything to).

Enjoy, and have fun!



Revision history:

0.8  Current version, conversion to 1.5/1.6, new skeletons
0.7  Updates to some skeletons
0.65 Resolved bugs with the Campaign map animations 
0.6  Updated to work with Vercingetorix' XIDX
0.5  Original release


Credits:
The primary credit and thanks for my release go to CA, both because of the great game 
they have created, and the wonderful base animations which formed an invaluable 
foundation for this project. Literally, they did 90% here (and I did a LOT myself, so 
that says how big their contribution was).

The second credit and thanks go to Vercingetorix, without whose wonderful programs 
none of this would be possible. He literally made it possible, so treat him nicely 
and send him cookies whenever you can.