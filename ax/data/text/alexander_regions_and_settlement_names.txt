¬ Regions names generated by Romans Campaign Editor
{Aornos}						Aornos
{Upper_Indus_Valley}			Upper Indus valley
{Surashtra}						Surashtra
{Girnar}						Girnar
{Chaonia}			Chaonia
{Pelion}			Pelion
{Ardiaia}			Ardiaia
{Skodra}			Skodra
{Dardania}			Dardania
{Naissos}			Naissos
{Skythia}			Skythia
{Tanais}			Tanais
{Mikra_Skythia}			Mikra Skythia
{Olbia}			Olbia
{Sarmatia}			Sarmatia
{Azara}			Azara
{Dahae}			Dahae
{Nisa}			Nisa
{Massagetae}			Massagetae
{Kucha}			Kucha
{Triballia}			Triballia
{Palatiolon}		Palatiolon
{Paionia}			Paionia
{Bylazora}			Bylazora
{Thrace}			Thrace
{Phillipopolis}		Phillipopolis
{Propontis}			Propontis
{Byzantion}			Byzantion
{Macedon}			Macedon
{Pella}				Pella
{Epirus}			Epirus
{Passaron}			Passaron
{Sogdiana}			Sogdiana
{Chach}			Chach
{Baktria}			Baktria
{Baktra}			Baktra
{Paphlagonia}			Paphlagonia
{Sinope}			Sinope
{Thospia}			Thospia
{Ayrarat}			Ayrarat
{Trapezous}			Trapezous
{Mikra_Armenia}			Mikra Armenia
{Melitene}			Melitene
{Armenia_Mesopotamia}		Armenia Mesopotamia
{Van}			Van
{Armavir}			Armavir
{Amida}			Amida
{Mazaka}			Mazaka
{Kappadokia}			Kappadokia
{Issus}			Issus
{Lydia}			Lydia
{Sardis}			Sardis
{Halikarnassos}			Halikarnassos
{Karia}			Karia
{Tarsos}			Tarsos
{Kilikia}			Kilikia
{Attika}			Attika
{Athens}			Athens
{Lakonia}			Lakonia
{Sparta}			Sparta
{Thessaly}			Thessaly
{Larissa}			Larissa
{Crete}				Crete
{Knossos}			Knossos
{Phrygia}			Phrygia
{Gordion}			Gordion
{Lampsacus}			Lampsacus
{Parthia}			Parthia
{Tabae}			Tabae
{Media}			Media
{Ekbatana}			Ekbatana
{India}			India
{Taxila}			Taxila
{Syria}			Syria
{Damaskos}			Damaskos
{Phoenikia}			Phoenikia
{Tyros}			Tyros
{Tyre}			Tyre
{Old_Tyre}			Old Tyre
{Assyria}			Assyria
{Thapsakos}			Thapsakos
{Adiabene}			Adiabene
{Arbela}			Arbela
{Aria}			Aria
{Arabia}			Arabia
{Petra}			Petra
{Babylonia}			Babylonia
{Babylon}			Babylon
{Elymais}			Elymais
{Susa}			Susa
{Siwa}			Siwa
{Lybia}			Lybia
{Marmarika}			Marmarika
{Rhakotis}			Rhakotis
{Upper_Egypt}			Upper Egypt
{Thebes2}			Thebes
{Lower_Egypt}			Lower Egypt
{Memphis}			Memphis
{Gedrosia}			Gedrosia
{Poura}			Poura
{Persis}			Persis
{Persepolis}			Persepolis
{Karmania}			Karmania
{Karmana}			Karmana
{Sinai}			Sinai
{Gaza}			Gaza
{Salamis}			Salamis
{Cyprus}			Cyprus
{Barkania}			Barkania
{Barke}				Barke
{Kyrenaika}			Kyrenaika
{Kyrene}			Kyrene
{Parthia}			Parthia
{Hekatompylos}			Hekatompylos
{Hyrkania}			Hyrkania
{Zadrakarta}			Zadrakarta
{Drangiania}			Drangiania
{Zranka}			Zranka
{Arachosia}			Arachosia
{Arachotos}			Arachotos
{Baktria}			Baktria
{Baktra}			Baktra
{Parapamisos}			Parapamisos
{Ortospana}			Ortospana
{Sattagydia}			Sattagydia
{Taxila}			Taxila
{Gandara}			Gandara
{Peukela}			Peukela
{Oreitia}			Oreitia
{Rambakia}			Rambakia
{Aria}				Aria
{Artakoana}			Artakoana
{Margiane}			Margiane
{Margos}			Margos
{Ferghana_Valley}	Ferghana Valley
{Kyroupolis}		Kyroupolis
{Sogdian_Highland}	Sogdian Highland
{Sogdian_Rock}		Sogdian Rock
{Central_Sogdia}	Central Sogdia
{Marakanda}			Marakanda
{Assakenia}			Assakenia
{Barygaza}			Barygaza
{Pauravas}			Pauravas 
{Sagala}			Sagala
{Oxydrakia}			Oxydrakia
{Kotkamalia}		Kotkamalia
{Mallia}			Mallia
{Multan}			Multan
{India}				India
{Sindomana}			Sindomana
{Sabrakia}			Sabrakia
{Pattala}			Pattala
{Taurike}			Taurike
{Chersonesos}		Chersonesos
{Kimmeria}			Kimmeria
{Paitikapaion}		Paitikapaion
{Hypanis}			Hypanis
{Phanagoreia}		Phanagoreia
{Sindike}			Sindike
{Gorgippa}			Gorgippa
{Iazartes}			Iazartes
{Tanais}			Tanais
{Ardhan}			Ardhan
{Kabalaka}			Kabalaka
{Kotais}			Kotais
{Kolchis}			Kolchis
{Tyragetia}						Tyragetia
{Klepidava}						Klepidava
{Kontensia}						Kontensia
{Piroboridava}					Piroboridava
{Ratakensia}					Ratakensia
{Albokensia}					Albokensia
{Argedava}						Argedava
{Buridavensia}					Buridavensia
{Buridava}						Buridava
{Getia}							Getia
{Helis}							Helis
{Karpia}						Karpia
{Karsidava}						Karsidava
{Ratakensia}					Ratakensia
{Markodava}						Markodava
{Dacia}							Dacia
{Dakidava}						Dakidava
{Kotinia}						Kotinia
{Kotinopolis}					Kotinopolis
{Anartia}						Anartia
{Anartopolis}					Anartopolis
{Pannonia}						Pannonia
{Singidunon}					Singidunon
{Mikra_Skythia}					Mikra Skythia
{Olbia}							Olbia
{Katiaroi}						Katiaroi
{Saron}							Saron
{Scythia}						Scythia
{Skythiapolis}					Skythiapolis
{Roxalanoi}						Roxalanoi
{Serimon}						Serimon
{Iazyges}						Iazyges
{Gelonos}						Gelonos
{Maiotis}						Maiotis
{Navaris}						Navaris
{Sirakes}						Sirakes
{Seraka}						Seraka
{Aorsoi}						Aorsoi
{Hexapolis}						Hexapolis
{Parnia}						Parnia
{Nisa}							Nisa
{Pissuria}						Pissuria
{Stratopedon_ton_Pissuroi}		Stratopedon ton Pissuroi
{Xanthia}						Xanthia
{Stratopedon_ton_Xanthioi}		Stratopedon ton Xanthioi
{Massagetai}					Massagetai
{Bukharak}						Bukharak
{Sahara}						Sahara
{Augila}						Augila
{Southern_Oasis}				Southern Oasis
{Mothis}						Mothis
{Maskat}						Maskat
{Regma}							Regma
{Chorasmene}					Chorasmene
{Chorasmia}						Chorasmia
{Sarmatia}						Sarmatia
{Chaurana}						Chaurana
{Lekhien}						Lekhien
{Hegra}							Hegra
{Gerrhaia_Arabia}				Gerrhaia Arabia
{Gera}						Gera
{Ligaia}						Ligaia
{Stratopedon_ton_Ligai}			Stratopedon ton Ligai
{Aspasia}						Aspasia
{Stratopedon_ton_Aspisoi}		Stratopedon ton Aspisoi
{Thyssagetai}					Thyssagetai
{Stratopedon_ton_Thyssagetai}	Stratopedon ton Thyssagetai
{Taklamakan}					Taklamakan
{Kasia}							Kasia
{Issedonia}						Issedonia
{Chigu}							Chigu
{Jaxartes}						Jaxartes
{Stratopedon_ton_Sakai}			Stratopedon ton Sakai
{Arabia1}						Arabia1
{Thomala}						Thomala
{Arabia2}						Arabia2
{Iathrippa}						Iathrippa
{Arabia3}						Arabia3
{Madiama}						Madiama
{Arabia4}						Arabia4
{Dumatha}						Dumatha
{Arabia5}						Arabia5
{Makoraba}						Makoraba
{Arabia6}						Arabia6
{Kama}							Kama
{Arabia7}						Arabia7
{Ierakon_Kome}					Ierakon Kome
{Arabia8}						Arabia8
{Sarkoe}						Sarkoe
{Arabia9}						Arabia9
{Kanipsa}						Kanipsa
{Arabia10}						Arabia10
{Labris}						Labris
{Arabia11}						Arabia11
{Artemita}						Artemita
{Arabia12}						Arabia12
{Alata}							Alata
