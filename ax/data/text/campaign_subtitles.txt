¬ Localised file for battle descriptions

{Alexander_campaign_intro_01}
Following the assassination of Philip II of Macedon, his 20 year old son, Alexander, has taken the throne.

{Alexander_campaign_intro_02}
Victory at Chaeronea had united Hellas under Philip's banner, but in the confusion following his death, unrest is rife.

{Alexander_campaign_intro_03}
To the south several Greek city-states are now on the verge of revolting to the new King.

{Alexander_campaign_intro_04}
Up north several Barbarian tribes subdued by Philip have overthrown the Macedonian garissons.

{Alexander_campaign_intro_05}
Alexander must crush this defiance quickly.

{Alexander_campaign_intro_06}
Only once this is achieved can he look to carry through his father's plan to strike at the mighty Persian empire.

{Alexander_campaign_intro_07}
With the all-powerful Persian fleet patrolling the waters to the south, Alexander will need to pick his moment carefully.

{Alexander_campaign_intro_08}
Securing coastal settlements will deny the Persian ships the ability to refit, eventually lessening their effectiveness.

{Alexander_campaign_intro_09}
But Alexander cannot spare the men to garrison such settlements too heavily, and it may be necessary to slaughter those who might otherwise rise up against him.

{Alexander_campaign_intro_10}
To achieve his objective within his lifetime, Alexander will need to maintain his momentum as he drives into the heart of Persia.

{Alexander_campaign_intro_11}
Reinforcements will take months to arrive, so he must recruit local troops to bolster his ranks.

{Alexander_campaign_intro_12}
In order to proclaim himself the rightful King of Asia, Alexander must control 25 key cities within the given turns.

{Alexander_campaign_outro_01}
You have reached the edge of the known world.

{Alexander_campaign_outro_02}
In short timespan you have conquered an empire stretching from Macedon to India.

{Alexander_campaign_outro_03}
This matched the achievement of the greatest conqueror the world has ever known - a man who’s name echoes throughout history!

{Alexander_campaign_outro_04}
However after his costly victory at the Hydaspes River, his men, exhausted, homesick and unsettled by their Kings bouts of increasingly unbalanced behaviour finally refused to go any further.

{Alexander_campaign_outro_05}
Despite his fury, Alexander had no choice but to lead his tattered troops down the Indus river where he encountered other warlike Indian tribes such as the malloi, which is said to be tho fiercest tribe in the region.

{Alexander_campaign_outro_06}
They wouldn't let him pass and after several skirmishes he reached the Mallian capital, Multan which he besieged.

{Alexander_campaign_outro_07}
During the siege he took an arrow through the chest when he was fighting on the city walls. This triggered a violent storm and the settlement was taken quickly.

{Alexander_campaign_outro_08}
Rumours of Alexander's death caused his soldiers to exterminate the city's entire population

{Alexander_campaign_outro_09}
But alexander survived thanks to a surgion named Kritomedos who removed the arrowhead from his chest.

{Alexander_campaign_outro_10}
Next he marched west toward Babylon where hundreds of his men perished in the Gedrosian desert.

{Alexander_campaign_outro_11}
Babylon would be the new centre of Macedon rule, further steps would be taken to create an homogenous culture and a new fleet would be born to challenge Arabia and Carthage.

{Alexander_campaign_outro_12}
But none of this was to be…

{Alexander_campaign_outro_13}
On his return to Babylon, Alexander finally met an opponent against whom all of his courage and skill was useless.

{Alexander_campaign_outro_14}
He succumbed to a fever and at the age of thirty-two the man who rode unscathed from countless battlefields became mortally sick.

{Alexander_campaign_outro_15}
While on his deathbed, his companions asked to whom he bequeathed his kingdom.

{Alexander_campaign_outro_16}
His laconic reply was: “to the strongest”.

{Alexander_campaign_outro_17}
Then he died.

{Alexander_campaign_outro_18}
Consequently his generals became rivals to the throne before the corpse was even cold.

{Alexander_campaign_outro_19}
This resulted in the Diadochi wars and the fragmentation of his empire.

{Alexander_campaign_outro_20}
Nonetheless Alexander had earned his epithet ‘The Great’ and in the process attained immortality!
